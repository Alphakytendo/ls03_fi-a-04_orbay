package view.menue;

import javax.swing.JFrame;
import java.awt.Component;
import java.awt.Container;

import model.User;

import javax.swing.JLabel;
import javax.swing.JTextField;

import controller.AlienDefenceController;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.awt.event.ActionEvent;

//TODO create a usermanagement
@SuppressWarnings("serial")
public class CreateUserWindow extends JFrame {
	private JTextField tfdName;
	private JTextField tfdNachname;
	private JTextField tfdGeburtstag;
	private JTextField tfdStra�e;
	private JTextField tfdHausnummer;
	private JTextField tfdPostleitzahl;
	private JTextField tfdStadt;
	private JTextField tfdLoginname;
	private JTextField tfdPasswort;
	private JTextField tfdGehaltsvorstellung;
	private JTextField tfdFamilienstand;
	private JTextField tfdEndnote;
	private AlienDefenceController alienDefenceController;
	public CreateUserWindow(AlienDefenceController alienDefenceController) {
		this.alienDefenceController = alienDefenceController;
		getContentPane().setLayout(null);
		
		JLabel lblName = new JLabel("Name:");
		lblName.setBounds(15, 61, 69, 20);
		getContentPane().add(lblName);
		
		JLabel lblNachname = new JLabel("Nachname:");
		lblNachname.setBounds(15, 109, 101, 20);
		getContentPane().add(lblNachname);
		
		JLabel lblGeburtstag = new JLabel("Geburtstag:");
		lblGeburtstag.setBounds(15, 158, 101, 20);
		getContentPane().add(lblGeburtstag);
		
		JLabel lblStra�e = new JLabel("Stra\u00DFe:");
		lblStra�e.setBounds(15, 215, 69, 20);
		getContentPane().add(lblStra�e);
		
		JLabel lblHausnummer = new JLabel("Hausnummer:");
		lblHausnummer.setBounds(15, 269, 101, 20);
		getContentPane().add(lblHausnummer);
		
		JLabel lblStadt = new JLabel("Stadt:");
		lblStadt.setBounds(15, 387, 69, 20);
		getContentPane().add(lblStadt);
		
		JLabel lblErstellenSieEinen = new JLabel("Erstellen Sie einen neuen User:");
		lblErstellenSieEinen.setBounds(82, 16, 226, 20);
		getContentPane().add(lblErstellenSieEinen);
		
		tfdName = new JTextField();
		tfdName.setBounds(191, 58, 215, 26);
		getContentPane().add(tfdName);
		tfdName.setColumns(10);
		
		tfdNachname = new JTextField();
		tfdNachname.setBounds(191, 106, 215, 26);
		getContentPane().add(tfdNachname);
		tfdNachname.setColumns(10);
		
		tfdGeburtstag = new JTextField();
		tfdGeburtstag.setBounds(191, 155, 215, 26);
		getContentPane().add(tfdGeburtstag);
		tfdGeburtstag.setColumns(10);
		
		tfdStra�e = new JTextField();
		tfdStra�e.setBounds(191, 212, 215, 26);
		getContentPane().add(tfdStra�e);
		tfdStra�e.setColumns(10);
		
		tfdHausnummer = new JTextField();
		tfdHausnummer.setBounds(191, 266, 215, 26);
		getContentPane().add(tfdHausnummer);
		tfdHausnummer.setColumns(10);
		
		tfdPostleitzahl = new JTextField();
		tfdPostleitzahl.setBounds(191, 326, 215, 26);
		getContentPane().add(tfdPostleitzahl);
		tfdPostleitzahl.setColumns(10);
		
		tfdStadt = new JTextField();
		tfdStadt.setBounds(191, 384, 215, 26);
		getContentPane().add(tfdStadt);
		tfdStadt.setColumns(10);
		
		tfdLoginname = new JTextField();
		tfdLoginname.setBounds(191, 447, 215, 26);
		getContentPane().add(tfdLoginname);
		tfdLoginname.setColumns(10);
		
		tfdPasswort = new JTextField();
		tfdPasswort.setBounds(191, 506, 215, 26);
		getContentPane().add(tfdPasswort);
		tfdPasswort.setColumns(10);
		
		tfdGehaltsvorstellung = new JTextField();
		tfdGehaltsvorstellung.setBounds(191, 563, 215, 26);
		getContentPane().add(tfdGehaltsvorstellung);
		tfdGehaltsvorstellung.setColumns(10);
		
		tfdFamilienstand = new JTextField();
		tfdFamilienstand.setBounds(191, 617, 215, 26);
		getContentPane().add(tfdFamilienstand);
		tfdFamilienstand.setColumns(10);
		
		JLabel lblLoginname = new JLabel("Loginname:");
		lblLoginname.setBounds(15, 450, 101, 20);
		getContentPane().add(lblLoginname);
		
		JLabel lblPasswort = new JLabel("Passwort:");
		lblPasswort.setBounds(15, 509, 69, 20);
		getContentPane().add(lblPasswort);
		
		JLabel lblGehaltsvorstellung = new JLabel("Gehaltsvorstellung:");
		lblGehaltsvorstellung.setBounds(15, 566, 161, 20);
		getContentPane().add(lblGehaltsvorstellung);
		
		JLabel lblFamilienstand = new JLabel("Familienstand:");
		lblFamilienstand.setBounds(15, 620, 120, 20);
		getContentPane().add(lblFamilienstand);
		
		JLabel lblEndnote = new JLabel("Endnote:");
		lblEndnote.setBounds(15, 668, 69, 20);
		getContentPane().add(lblEndnote);
		
		JLabel lblPostleitzahl = new JLabel("Postleitzahl:");
		lblPostleitzahl.setBounds(15, 329, 101, 20);
		getContentPane().add(lblPostleitzahl);
		
		tfdEndnote = new JTextField();
		tfdEndnote.setBounds(191, 665, 215, 26);
		getContentPane().add(tfdEndnote);
		tfdEndnote.setColumns(10);
		
		JButton btnNewButton = new JButton("Neuen User anlegen");
		btnNewButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				NeuenBenutzerAnlegen();
			}
		});
		btnNewButton.setBounds(15, 739, 175, 29);
		getContentPane().add(btnNewButton);
		
		
		JButton btnNewButton_1 = new JButton("Abbrechen");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AnlegenAbbrechen();
			}			
		});
		btnNewButton_1.setBounds(236, 739, 115, 29);
		getContentPane().add(btnNewButton_1);
	}
	
	private void AnlegenAbbrechen() {
		this.dispose();
		
	}

	private void NeuenBenutzerAnlegen() {
		boolean feldersindleer = false;
		for (Component c : ((Container) getContentPane()).getComponents()) {
			if (c instanceof JTextField) {
				User user = this.readDataToUser();
				this.alienDefenceController.getUserController().createUser(user);
				this.dispose();
			} 
		}
	}

	private User readDataToUser() {

		return new User(0,
				tfdName.getText(),
				tfdNachname.getText(),
				this.parseDate(tfdGeburtstag.getText()),
				tfdStra�e.getText(),
				tfdHausnummer.getText(),
				tfdPostleitzahl.getText(),
				tfdStadt.getText(),
				tfdLoginname.getText(),
				tfdPasswort.getText(),
				Integer.parseInt(tfdGehaltsvorstellung.getText()),
				tfdFamilienstand.getText(),
				Double.parseDouble(tfdEndnote.getText())
				);
				
	}

	private LocalDate parseDate(String date) { 
	
		String pattern = "dd.MM.yyyy";
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
		return LocalDate.parse(date, formatter);
	}
	
}
