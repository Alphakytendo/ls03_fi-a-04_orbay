package view.menue;

import controller.AlienDefenceController;
import model.persistance.IPersistance;
import model.persistanceDB.PersistanceDB;
import model.persistanceDummy.PersistanceDummy;

public class StartAlienDefence {

	public static void main(String[] args) {
		
		IPersistance 		   alienDefenceModel      = new PersistanceDB();
		AlienDefenceController alienDefenceController = new AlienDefenceController(alienDefenceModel);
		MainMenue              mainMenue              = new MainMenue(alienDefenceController);
		
		mainMenue.setVisible(true);
	}
}
