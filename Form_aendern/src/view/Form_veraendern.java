package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Form_veraendern extends JFrame {

	private JPanel pnlHintergrund;
	private JTextField txtHierBitteText;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Form_veraendern frame = new Form_veraendern();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Form_veraendern() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 630, 902);
		pnlHintergrund = new JPanel();
		pnlHintergrund.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(pnlHintergrund);
		pnlHintergrund.setLayout(null);
		
		JLabel lblDieserTextSoll = new JLabel("Dieser Text soll ver\u00E4ndert werden.");
		lblDieserTextSoll.setBounds(25, 16, 555, 108);
		pnlHintergrund.add(lblDieserTextSoll);
		
		JLabel lblAufgabeHintergrundfarbe = new JLabel("Aufgabe 1: Hintergrundfarbe \u00E4ndern");
		lblAufgabeHintergrundfarbe.setBounds(15, 140, 349, 20);
		pnlHintergrund.add(lblAufgabeHintergrundfarbe);
		
		JButton btnRot = new JButton("Rot");
		btnRot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				pnlHintergrund.setBackground(Color.RED);
			}
		});
		btnRot.setBounds(15, 176, 158, 29);
		pnlHintergrund.add(btnRot);
		
		JButton btnGrün = new JButton("Gr\u00FCn");
		btnGrün.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pnlHintergrund.setBackground(Color.GREEN);
			}
		});
		btnGrün.setBounds(204, 176, 158, 29);
		pnlHintergrund.add(btnGrün);
		
		JButton btnBlau = new JButton("Blau");
		btnBlau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pnlHintergrund.setBackground(Color.BLUE);
			}
		});
		btnBlau.setBounds(396, 176, 158, 29);
		pnlHintergrund.add(btnBlau);
		
		JButton btnGelb = new JButton("Gelb");
		btnGelb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pnlHintergrund.setBackground(Color.YELLOW);
			}
		});
		btnGelb.setBounds(15, 221, 158, 29);
		pnlHintergrund.add(btnGelb);
		
		JButton btnStandardfarbe = new JButton("Standardfarbe");
		btnStandardfarbe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pnlHintergrund.setBackground(UIManager.getColor("menu"));
			}
		});
		btnStandardfarbe.setBounds(206, 221, 158, 29);
		pnlHintergrund.add(btnStandardfarbe);
		
		JButton btnFarbe_auswählen = new JButton("Farbe ausw\u00E4hlen");
		btnFarbe_auswählen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pnlHintergrund.setBackground(JColorChooser.showDialog(lblDieserTextSoll, "Neue Farbe wählen", Color.white));
			}
		});
		btnFarbe_auswählen.setBounds(396, 221, 158, 29);
		pnlHintergrund.add(btnFarbe_auswählen);
		
		JLabel lblAufgabeText = new JLabel("Aufgabe 2: Text formatieren");
		lblAufgabeText.setBounds(15, 274, 224, 20);
		pnlHintergrund.add(lblAufgabeText);
		
		JButton btnArial = new JButton("Arial");
		btnArial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Font alterFont = lblDieserTextSoll.getFont();
				Font arial = new Font("Arial", alterFont.getStyle(), alterFont.getSize());
				lblDieserTextSoll.setFont(arial);
			}
		});
		btnArial.setBounds(15, 310, 141, 29);
		pnlHintergrund.add(btnArial);
		
		JButton btnComic_Sans_MS = new JButton("Comic Sans MS");
		btnComic_Sans_MS.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Font alterFont = lblDieserTextSoll.getFont();
				Font comicsans = new Font("Comic Sans MS", alterFont.getStyle(), alterFont.getSize());
				lblDieserTextSoll.setFont(comicsans);
			}
		});
		btnComic_Sans_MS.setBounds(171, 310, 149, 29);
		pnlHintergrund.add(btnComic_Sans_MS);
		
		JButton btnCourier_New = new JButton("Courier New");
		btnCourier_New.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Font alterFont = lblDieserTextSoll.getFont();
				Font couriernew = new Font("Courier New", alterFont.getStyle(), alterFont.getSize());
				lblDieserTextSoll.setFont(couriernew);
			}
		});
		btnCourier_New.setBounds(335, 310, 149, 29);
		pnlHintergrund.add(btnCourier_New);
		
		txtHierBitteText = new JTextField();
		txtHierBitteText.setText("Hier bitte Text eingeben");
		txtHierBitteText.setBounds(15, 355, 421, 26);
		pnlHintergrund.add(txtHierBitteText);
		txtHierBitteText.setColumns(10);
		
		JButton btnIns_Label_schreiben = new JButton("Ins Label schreiben");
		btnIns_Label_schreiben.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String texteingabe = txtHierBitteText.getText();
				lblDieserTextSoll.setText(texteingabe);
			}
		});
		btnIns_Label_schreiben.setBounds(15, 397, 224, 29);
		pnlHintergrund.add(btnIns_Label_schreiben);
		
		JButton btnText_im_Label_löschen = new JButton("Text im Label l\u00F6schen");
		btnText_im_Label_löschen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setText("");
			}
		});
		btnText_im_Label_löschen.setBounds(294, 397, 224, 29);
		pnlHintergrund.add(btnText_im_Label_löschen);
		
		JLabel lblAufgabeSchirftfarbe = new JLabel("Aufgabe 3: Schirftfarbe \u00E4ndern");
		lblAufgabeSchirftfarbe.setBounds(15, 445, 289, 20);
		pnlHintergrund.add(lblAufgabeSchirftfarbe);
		
		JButton btnRot_Schriftart = new JButton("Rot");
		btnRot_Schriftart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setForeground(Color.RED);
			}
		});
		btnRot_Schriftart.setBounds(15, 492, 115, 29);
		pnlHintergrund.add(btnRot_Schriftart);
		
		JButton btnBlau_Schriftart = new JButton("Blau");
		btnBlau_Schriftart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setForeground(Color.BLUE);
			}
		});
		btnBlau_Schriftart.setBounds(171, 492, 115, 29);
		pnlHintergrund.add(btnBlau_Schriftart);
		
		JButton btnSchwarz_Schriftart = new JButton("Schwarz");
		btnSchwarz_Schriftart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setForeground(Color.BLACK);
			}
		});
		btnSchwarz_Schriftart.setBounds(321, 492, 115, 29);
		pnlHintergrund.add(btnSchwarz_Schriftart);
		
		JLabel lblSchriftgreVerndern = new JLabel("Aufgabe 4: Schriftgr\u00F6\u00DFe ver\u00E4ndern");
		lblSchriftgreVerndern.setBounds(25, 546, 279, 20);
		pnlHintergrund.add(lblSchriftgreVerndern);
		
		JButton btnPlus = new JButton("+");
		btnPlus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {				
				Font alterFont = lblDieserTextSoll.getFont();
				Font size = new Font(alterFont.getFamily(), alterFont.getStyle() , alterFont.getSize()+1 );
				lblDieserTextSoll.setFont(size);
			}
		});
		btnPlus.setBounds(35, 585, 115, 29);
		pnlHintergrund.add(btnPlus);
		
		JButton btnMinus = new JButton("-");
		btnMinus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Font alterFont = lblDieserTextSoll.getFont();
				Font size = new Font(alterFont.getFamily(), alterFont.getStyle() , alterFont.getSize()-1 );
				lblDieserTextSoll.setFont(size);
			}
		});
		btnMinus.setBounds(270, 585, 115, 29);
		pnlHintergrund.add(btnMinus);
		
		JLabel lblAufgabeTextausrichtung = new JLabel("Aufgabe 5: Textausrichtung");
		lblAufgabeTextausrichtung.setBounds(15, 645, 224, 20);
		pnlHintergrund.add(lblAufgabeTextausrichtung);
		
		JButton btnLinksbündig = new JButton("linksb\u00FCndig");
		btnLinksbündig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setHorizontalAlignment(SwingConstants.LEFT);
			}
		});
		btnLinksbündig.setBounds(15, 694, 141, 29);
		pnlHintergrund.add(btnLinksbündig);
		
		JButton btnZentriert = new JButton("zentriert");
		btnZentriert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setHorizontalAlignment(SwingConstants.CENTER);
			}
		});
		btnZentriert.setBounds(171, 694, 133, 29);
		pnlHintergrund.add(btnZentriert);
		
		JButton btnRechtsbündig = new JButton("rechtsb\u00FCndig");
		btnRechtsbündig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setHorizontalAlignment(SwingConstants.RIGHT);
			}
		});
		btnRechtsbündig.setBounds(321, 694, 141, 29);
		pnlHintergrund.add(btnRechtsbündig);
		
		JLabel lblAufgabeProgramm_beenden = new JLabel("Aufgabe 6: Programm beenden");
		lblAufgabeProgramm_beenden.setBounds(15, 755, 271, 20);
		pnlHintergrund.add(lblAufgabeProgramm_beenden);
		
		JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(1);
			}
		});
		btnExit.setBounds(171, 808, 115, 29);
		pnlHintergrund.add(btnExit);
	}

}
